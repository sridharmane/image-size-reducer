from PIL import Image
import os
import glob
from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument("-files",
                    help="file selection pattern", default="./*.jpg")
parser.add_argument("-out",
                    default="./export",
                    help="output directory")

args = parser.parse_args()

MAX_SIZE = 2500  # Mb
MAX_QUALITY = 95
MIN_QUALITY = 80
QUALITY_REDUCTION_FACTOR = 1

input_file = '1 (1).jpg'
output_file = '1.jpg'
output_file2 = '1-2.jpg'


def reduce_size(image: Image, path: str, quality=MAX_QUALITY):
    image.
    image.save(path, optimize=True, quality=quality)
    if (get_file_size(path) >= MAX_SIZE and quality >= MIN_QUALITY):
        return reduce_size(image, path, quality-QUALITY_REDUCTION_FACTOR)
    return (quality)


def get_file_size(path: str):
    return int(os.path.getsize(path)/1024)


input_images = glob.glob(args.files)
print(f" Found {len(input_images)} files matching {args.files}")

for image_path in input_images:
    file_name = image_path.split('\\')[-1]
    image = Image.open(image_path)
    if not os.path.isdir(args.out):
        os.makedirs(args.out)
    export_path = os.path.join(args.out, file_name)
    quality = reduce_size(image, export_path)
    print(
        f"{export_path}:\t\t{image.size} : {get_file_size(image_path)} -> {get_file_size(export_path)} : {quality}%")
